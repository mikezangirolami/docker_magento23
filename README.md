# Magento 2.3.x Docker Container
This Docker container is built with support of `Nginx (latest)`, `PHP-FPM 7.3.14` and `MySQL 5.7`.<br />
It is also shipped with `xDebug 2.9.4`.
## Prerequisites
- Linux:
    - Docker Daemon: Install via CLI: `sudo apt update;` <br />
    `sudo apt install apt-transport-https ca-certificates curl software-properties-common;` <br />
    `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -;`<br /> 
    `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable";` <br />
    `sudo apt update;`<br />
    `sudo apt install docker-ce`<br />
    `sudo groupadd docker`<br />
    `sudo usermod -aG docker $USER`<br />
    - Docker Compose: Install via CLI: `sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose; sudo chmod +x /usr/local/bin/docker-compose`<br />
    ^ You can substitute 1.25.4 with the last release [here](https://docs.docker.com/compose/release-notes/)<br />
    If you have trouble with docker or docker-compose execution (for example it requires sudo), try to close and reopen the terminal.
        
- macOS:
    - Docker Desktop: Install via [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-mac/)
 
- Windows:
    - Docker Desktop: Install via [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
    - GitBash: Install via [Git for Windows](https://gitforwindows.org/)
    <= I recommend to use this CLI in the various phases in Windows OS.
    
N.B: This Docker container has been tested on this OS and Docker versions:
- Ubuntu 18.04 / Docker Daemon v19.03.8 / Docker Compose v1.25.4

- MacOS Catalina 10.15.3 / Docker Daemon v19.03.8 / Docker Compose v1.25.4

- Windows 10 / Docker Daemon v19.03.8 / Docker Compose v1.25.4

## How to use this repository
- 1 - Clone this repository into your computer

- 2 - Open your CLI and move into the cloned project and move again into docker path with this command: `cd .dev/docker`

- 3 - If you are a Mac or Windows user, rename the file in `docker-compose.override.yml.winmac` into
`docker-compose.override.yml`, else skip to the next point.<br />
N.B: This docker container use cached logic volumes for MacOS. This is necessary to speed
the Magento platform execution, if you don't want this patch, remove the cached volumes in the override file.
Unfortunately, for Windows there aren't yet docker patches of this type available but this override file 
serves also for run xDebug.

- 4 - Run this command: `docker-compose up` or `docker-compose up -d` to run it in detached mode.
I suggest to run without detached mode for the first time, so you'll can see eventually warning or error logs.

- 5 - After this command, run in another terminal: `docker ps` to see the containers dragged up.
You should see a similar scenario:

| CONTAINER ID | IMAGE            | COMMAND                | CREATED     | STATUS     | PORTS                             | NAMES            |
| ------------ | ---------------- | ---------------------- | ----------- | ---------- | --------------------------------- | ---------------- |
| 42f3d3e49021 | magento23_nginx  | "nginx -g 'daemon of…" | 7 hours ago | Up 7 hours | 0.0.0.0:80->80/tcp                | magento23_nginx  |
| 40d3cd338a12 | magento23_phpfpm | "docker-php-entrypoi…" | 7 hours ago | Up 7 hours | 9000/tcp                          | magento23_phpfpm |
| 97ae0eec6d85 | magento23_mysql  | "docker-entrypoint.s…" | 7 hours ago | Up 7 hours | 0.0.0.0:3306->3306/tcp, 33060/tcp | magento23_mysql  |

- 6 - Now you can access into magento23_phpfpm container,
running this command: `docker exec -it --user 1000 magento23_phpfpm bash`
or as super-user (not recommended in this phase): `docker exec -it magento23_phpfpm bash`

- 7 - Before to proceed with Magento installation, you have to register a Magento account
at the [Magento Marketplace](https://marketplace.magento.com/)<br />
After the registration, you can login and go to _My profile > Access keys_ to generate and get the credentials
needed in the next step. Apart from this, I suggest you to login in your github account via browser,
because some magento modules could require a token via link during the installation.

- 8 - Install Magento 2 via composer in src folder (mapped as /var/www/html in magento23_phpfpm container), using
this command: `composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition .`
<br />N.B: This folder have to been emtpy, so check it using `ls -laF` command.<br />
If there are files (e.g: `.gitkeep`), delete them.<br />
Insert the keys previously created (public key as username and the private key as password) and wait composer to finish.

- 9 - Configure `m2.docker.local` as localhost in your machine (not in container). Updates this file as super-user:<br />
Linux: `/etc/hosts`<br />
MacOS: `/etc/hosts`<br />
Windows: `c:\Windows\System32\Drivers\etc\hosts`<br />
Once opened, add this row: `127.0.0.1 m2.docker.local` and save it.

- 10 - Here we are! Now you can access to Magento 2 via browser going to `m2.docker.local` host.<br />
Browser DNS Cache could give you problems, in this case, clear it or try going directly to: `http://m2.docker.local`<br />
Now you can start Magento 2 installation procedure.<br />
**Remember:** Set with the mysql data environment in docker-compose.yml file the Database configuration in second step:
<br />'Database Server Host': `mysql`,
<br />'Database Server Username': `root`,
<br />'Database Server Password': `iamtheonewhoknocks`
<br />'Database Name': `magento2`

- 11 - Finish the installation and Enjoy!

**N.B:** In case of you want use this docker in more project in your computer, you need to assign a different project
name as `COMPOSE_PROJECT_NAME` in `docker_magento23/.dev/docker/.env` file for each of projects.

## How to use xDebug with PHPStorm IDE
- 1 - Install xDebug Helper browser extension:
[Chrome](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc) or
[Firefox](https://addons.mozilla.org/en-US/firefox/addon/xdebug-helper-for-firefox/)<br />

- 2 - Go in xDebug Helper extension options and set IDE key as `PHPSTORM`. Now enable it doing click
on the bug icon and pressing 'Debug'.

- 3 - If you are a Windows or Mac user, remember to remove the extension `.winmac`
of `docker_magento23/.dev/docker/docker-compose.override.yml.winmac` file.
If your containers are already running, stop (`docker-compose stop`),
rebuild (`docker-compose build`) and restart (`docker-compose up -d`) them again, else skip to next point.

- 4 - Enable listening for PHP Debug Connections ("Phone" icon in top right)

- 5 - Go to _Settings (Preferences) > Languages & Frameworks > PHP > Debug :: Xdebug :: Debug Port_ and set **9001**

- 6 - Go to _Settings (Preferences) > Languages & Frameworks > PHP > Servers_ and press **+** button:<br />
'Name': `HTTP`<br>
'Host': `m2.docker.local`<br />
'Port': `80`<br />
'Debugger': `Xdebug`<br />
Check 'Use path mappings' and map `docker_magento23/src` as `/var/www/html`<br />
Press again **+** button:<br />
'Name': `HTTPS`<br>
'Host': `m2.docker.local`<br />
'Port': `443`<br />
'Debugger': `Xdebug`<br />
Check 'Use path mappings' and map `docker_magento23/src` as `/var/www/html`

- 7 - Add a breakpoint and enjoy with debugging!

N.B: If it doesn't works in Mac or Windows, try with the oldest trick in the world: restart your computer and try again.

## How to change PHP settings
This docker container starts shipped with a default php.ini configurations with 2G of memory_limit.<br />
If you want to edit php configurations, you can edit php.ini file stored in phpfpm folder and rebuild the containers:<br />
`docker-compose stop`<br />
`docker-compose build`<br />
`docker-compose up -d`<br />

## How to connect with MySQL
When the docker is up, you can:
Set user credentials using the `docker_magento23/.dev/docker/docker-compose.yml` file. You find the entries at mysql node.<br />
Connect from local machine (with Workbench for example), using the localhost address (127.0.0.1) and the credentials.<br />
Connect from another container, like from a framework running on php side, use the entry `mysql` as address/server.<br />

## How to use Grunt
Before to use this JS task runner via CLI you need to configure Grunt in your Magento project. If you don't know how do that,
there are plenty of sites explaining it. <br />
After that Grunt is configured, you can use it via magento23_phpfpm container:
- 1 - Enter in the container as root: `docker exec -it magento23_phpfpm bash`
- 2 - Run `npm i` to install the node modules
- 3 - Enter in the container as local user (`docker exec -it --user 1000 magento23_phpfpm bash`) and enjoy with Grunt :D

## Contribution
This is an open source module and can be used as a base for other modules. If you have suggestions for improvements,
just submit your pull request.

## Authors
[Michael Zangirolami](https://gitlab.com/mikezangirolami)

## License
This project is licensed under GNU General Public License, version 3.
